package ua.work;

public class UtilsForMatrix {

    private static int getSumOfElementsInRow(int []row){
        int result = 0;
        for (int element : row) {
            result+=element;
        }
        return result;
    }

    public static int getIndexRowMaximumSumElements(int [][]matrix){

        if(matrix==null||matrix.length == 0 ){
            return -1;
        }

        boolean flag=false;
        int maxSum = 0;
        int indexRow = -1;

        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i] != null && matrix[i].length > 0) {
                if(!flag){
                    flag=true;
                    maxSum=getSumOfElementsInRow(matrix[i]);
                    indexRow = i;
                }else{
                    int currentSum = getSumOfElementsInRow(matrix[i]);
                    if (currentSum > maxSum) {
                        maxSum = currentSum;
                        indexRow = i;
                    }
                }
            }
        }

        return indexRow;

    }
}
