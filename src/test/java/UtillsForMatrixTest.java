import org.junit.jupiter.api.Test;
import ua.work.UtilsForMatrix;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtillsForMatrixTest {
    @Test
    public void testGetIndexRowMaximumSumElements_NullMatrix() {
        int[][] matrix = null;
        assertEquals(-1, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return -1 for null matrix");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_EmptyMatrix() {
        int[][] matrix = {};
        assertEquals(-1, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return -1 for empty matrix");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_AllRowsEmpty() {
        int[][] matrix = { {}, {}, {} };
        assertEquals(-1, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return -1 for matrix with empty rows");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_SingleRow() {
        int[][] matrix = { {1, 2, 3} };
        assertEquals(0, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 0 for single-row matrix");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_MultipleRows() {
        int[][] matrix = {
                {1, 2, 3},     // sum: 6
                {4, 5, 6},     // sum: 15
                {7, 8, 9},     // sum: 24
                {10, 11, 12}   // sum: 33
        };
        assertEquals(3, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 3 for the row with the maximum sum");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_ContainsNullRows() {
        int[][] matrix = {
                {1, 2, 3},  // sum: 6
                null,
                {4, 5},     // sum: 9
                {6, 7, 8}   // sum: 21
        };
        assertEquals(3, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 3 for the row with the maximum sum, ignoring null rows");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_ContainsEmptyRows() {
        int[][] matrix = {
                {1, 2, 3},  // sum: 6
                {},         // empty row
                {4, 5},     // sum: 9
                {6, 7, 8}   // sum: 21
        };
        assertEquals(3, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 3 for the row with the maximum sum, ignoring empty rows");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_ContainsNegative() {
        int[][] matrix = {
                {-1, -2, -3},  // sum: -6
                {},         // empty row
                {-4, -5},     // sum: -9
                {-6, -7, -8}   // sum: -21
        };
        assertEquals(0, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 0 for the row with the maximum sum, ignoring empty rows");
    }

    @Test
    public void testGetIndexRowMaximumSumElements_EqualSumRows() {
        int[][] matrix = {
                {1, 1, 1},  // sum: 3
                {2, 2, 2},  // sum: 6
                {3, 3},     // sum: 6
                {0, 6}      // sum: 6
        };
        assertEquals(1, UtilsForMatrix.getIndexRowMaximumSumElements(matrix),
                "Method should return 1 for the first row with the maximum sum if there are multiple such rows");
    }
}


